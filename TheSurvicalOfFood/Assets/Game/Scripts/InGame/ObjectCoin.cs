using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectCoin : MonoBehaviour
{
    [SerializeField] ParticleSystem m_effect;
    // Start is called before the first frame update
    private Transform m_target;
    private bool m_stop;
    private Vector3 temp = Vector3.zero;
    
    private void OnEnable()
    {
        transform.GetComponent<SpriteRenderer>().DOFade(1, 0.01f);
        m_effect.gameObject.SetActive(false);
        StopAllCoroutines();
        m_stop = false;
        GameController.restartGame += ResetObject;
        //GameController.finishEvent += DestroyGame;
    }
    private void Awake()
    {
        
    }
    private void OnDestroy()
    {
        GameController.restartGame -= ResetObject;
        //GameController.finishEvent -= DestroyGame;
    }
    void Start()
    {
        m_stop = false;
        //m_player = GameObject.FindGameObjectWithTag(GameConstant.TAG_PLAYER).GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_target == null || m_stop)
            return;
        transform.position = Vector3.SmoothDamp(transform.position, m_target.transform.position, ref temp, 0.3f);//Vector3.MoveTowards(transform.position, m_target.transform.position, 10f);//Vector3.Lerp(transform.position, m_target.transform.position, 3f);

    }
    private void DestroyGame()
    {
        //Debug.Log("finish-coin");
        StartCoroutine(WaitOnDestroy());
        //StopAllCoroutines();
    }
    private void ResetObject()
    {
        Destroy(gameObject,0.1f);
    }
    IEnumerator WaitOnDestroy()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(gameObject);
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == null || m_stop)
            return;
        if (collider.tag == GameConstant.TAG_MAGNET)
        {
            m_target = collider.transform;
        }
        if (collider.tag == GameConstant.TAG_PLAYER)
        {
            m_stop = true;
            GameController.UpdateLevelCoin(1);
            DoTrigger();
            

        }
    }
    private void DoTrigger()
    {
        //Debug.Log("train");
        transform.GetComponent<SpriteRenderer>().DOFade(0, 0.01f);
        m_effect.gameObject.SetActive(true);
        m_effect.Play();
        Destroy(gameObject, 1f);
    }

}
