using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class InventoryPanelWinGame : MonoBehaviour
{
    [SerializeField] private List<Sprite> m_listSprites;
    [SerializeField] private List<GameObject> m_listButtons;
    [SerializeField] private List<GameObject> m_listItem;
    [SerializeField] private List<GameObject> m_listWeapon;
    // Start is called before the first frame update

    private List<int> m_listValues = new List<int>(); // luu 3 gia tri cua ButtonItem
    //private int m_indexButton0 = 0;
    //private int m_indexButton1 = 0;
    //private int m_indexButton2 = 0;
   
    private void OnEnable()
    {
        GameController.updateWeaponUIEvent += UpdateWeapon;
        GameController.updateItemUIEvent += UpdateItem;
    }
    private void OnDisable()
    {
        GameController.updateWeaponUIEvent -= UpdateWeapon;
        GameController.updateItemUIEvent -= UpdateItem;
    }
    void Start()
    {
        //Debug.Log("M--------" + MainModel.arrItemTotals.Length);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Init(bool Win = false)
    {
        //Debug.Log("M--------" + MainModel.arrWeaponTotals.Length);
        if(Win)
        {
            RandomItemButton();
        }
        SetupImageListWeapon();
        SetupImageListItem();
        SetButton(Win);
    }
    private void UpdateWeapon(int index, bool buy) // gioi han list = 6 phan tu
    { // show listMainmodel -> setupImage
        //Debug.Log("M--------" + MainModel.arrWeaponTotals.Length);
      
        SetupImageListWeapon();
    }
    private void SetupImageListWeapon()
    {
        for(int i=0; i< MainModel.arrWeaponTotals.Length;i++)
        {
            Image img = m_listWeapon[i].transform.Find("icon").GetComponent<Image>();
            img.gameObject.SetActive(false);
            if (MainModel.arrWeaponTotals[i] != -1)
            {
                img.sprite = m_listSprites[MainModel.arrWeaponTotals[i]];
                img.gameObject.SetActive(true);
            }
        }   
    }
    private void UpdateItem(int index, bool buy) // gioi han list = 12 phan tu
    { // show listMainmodel -> setupImage
       // Debug.Log("M--------" + MainModel.arrItemTotals.Length);
       
        SetupImageListItem();
    }
    private void SetupImageListItem()
    {
        for (int i = 0; i < MainModel.arrItemTotals.Length; i++)
        {
            Image img = m_listItem[i].transform.Find("icon").GetComponent<Image>();
            img.gameObject.SetActive(false);
            if (MainModel.arrItemTotals[i] != -1)
            {
                img.sprite = m_listSprites[MainModel.arrItemTotals[i]];
                img.gameObject.SetActive(true);
            }
        }
    }
   
    private void SetButton(bool Win)
    {
        foreach (var item in m_listButtons)
            item.SetActive(Win);
    }
    private void RandomItemButton()
    {
        m_listValues = new List<int>();
        int index = 0;

        while (index < 3)
        {
            int random = Random.RandomRange(0, m_listSprites.Count);
            if (!m_listValues.Contains(random))
            {
                m_listValues.Add(random);
                index++;
            }

        }
        SetupItemButtonInit();
    }
    private void SetupItemButtonInit()
    {
        for (int i = 0; i < m_listValues.Count; i++)
        {
            GameObject item = m_listButtons[i].transform.Find("icon").gameObject;
            SetupImageItemButton(item, i);
        }
    }
    private void SetupImageItemButton(GameObject item, int index)
    {

        Image img = m_listButtons[index].transform.Find("icon").GetComponent<Image>();
        TextMeshProUGUI text = m_listButtons[index].transform.Find("text/text-value").GetComponent<TextMeshProUGUI>();
        switch (index.ToString())
        {
            case "0": 
                img.sprite = m_listSprites[m_listValues[index]];
                text.text = "+10 Amor";
                
                break;
            case "1": 
                img.sprite = m_listSprites[m_listValues[index]];
                text.text = "+5 Critical Chance";
                
                break;
            case "2": 
                img.sprite = m_listSprites[m_listValues[index]];
                text.text = "+25 Critical Damage";
              
                break;
        }
    
    }
    public void ButtonItem0OnClick() // khi nhan button -> them vao list weapon or item -> update weapon or item 
    {
        //m_listValues[0];
        GameController.BuyWeapon(m_listValues[0],-1, true);
        m_listButtons[0].SetActive(false);
    }
    public void ButtonItem1OnClick() // khi nhan button -> them vao list weapon or item -> update weapon or item 
    {
        //m_listValues[0];
        GameController.BuyWeapon(m_listValues[1],-1, true);
        m_listButtons[1].SetActive(false);
    }
    public void ButtonItem2OnClick() // khi nhan button -> them vao list weapon or item -> update weapon or item 
    {
        //m_listValues[0];
        GameController.BuyWeapon(m_listValues[2],-1, true);
        m_listButtons[2].SetActive(false);
    }
}
