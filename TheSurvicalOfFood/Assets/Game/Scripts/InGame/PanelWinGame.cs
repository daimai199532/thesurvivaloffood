using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class PanelWinGame : MonoBehaviour
{
    [SerializeField] private List<Sprite> m_listSprites;
    [SerializeField] private List<GameObject> m_listProperties;
    [SerializeField] private List<GameObject> m_listItems;
    [SerializeField] private Toggle m_toggleStats;
    [SerializeField] private Toggle m_toggleInventory;
    [SerializeField] private GameObject m_bntGo;
    [SerializeField] private GameObject m_buttonGoWin;
    [SerializeField] private GameObject m_contentStats;
    [SerializeField] private GameObject m_contentInventory;
    

    private List<int> m_listValues = new List<int>();
    private int m_valueItem1 = 0;
    private int m_valueItem2 = 0;
    private int m_valueItem3 = 0;
    private void OnEnable()
    {
        GameController.updatePropertiesEvent += UpdateTextProperties;
    }
    private void OnDisable()
    {
        GameController.updatePropertiesEvent -= UpdateTextProperties;
    }

   
    public void Init( bool isWin = false)
    {
        if(isWin)
        {
            RandomItem();
            SetupItemInit();
            for (int i = 0; i < m_listProperties.Count; i++)
                UpdateTextProperties(i, 0);
            ToggleStats();
        }
        else
        {
            ToggleInventory();
        }
        OnActiveListItems(isWin);

    }
    private void UpdateTextProperties(int index, int value)
    {
       
        TextMeshProUGUI text = m_listProperties[index].transform.Find("text").GetComponent<TextMeshProUGUI>();
        TextMeshProUGUI textValue = m_listProperties[index].transform.Find("propertie-number-text").GetComponent<TextMeshProUGUI>();
        switch (index.ToString())
        {
            case "0":
                //Debug.Log((MainModel.GetIsChangeProperties(MainModel.isAmorPlayer.ToString()) == "True") + "====================");
                if (MainModel.GetIsChangeProperties(MainModel.isAmorPlayer.ToString()) == "True")
                {
                    text.color = Color.red;
                    textValue.color = Color.red;
                }
                textValue.text = MainModel.amorPlayer.ToString();
               
                break;
            case "1":
                if (MainModel.GetIsChangeProperties(MainModel.isCriticalChancePlayer.ToString()) == "True")
                {
                    text.color = Color.red;
                    textValue.color = Color.red;
                }
                textValue.text = MainModel.criticalChancePlayer.ToString();
               
                break;
            case "2":
                if (MainModel.GetIsChangeProperties(MainModel.isCriticalDamagePlayer.ToString()) == "True")
                {
                    text.color = Color.red;
                    textValue.color = Color.red;
                }   
                textValue.text = MainModel.criticalDamagePlayer.ToString();
              
                break;
        }
    }
    private void OnAnimNumber()
    {

    }
    public  void ToggleStats()
    {
        m_toggleInventory.isOn = false;
        m_toggleStats.isOn= true; // dang bug tai day
        HiddenContent();
        m_contentStats.SetActive(true);
    }
    public void ToggleInventory(bool isWin= false)
    {
        m_toggleStats.isOn = false;
        //m_toggleInventory.isOn = true; // dang bug tai day
        HiddenContent();
        m_contentInventory.SetActive(true);
        m_contentInventory.GetComponent<InventoryPanelWinGame>().Init(isWin);
    }
   
    private void HiddenContent()
    {
        m_contentInventory.SetActive(false);
        m_contentStats.SetActive(false); 
    }
    private void RandomItem()
    {
        m_listValues = new List<int>();
        int index = 0;
        
        while(index < 3)
        {
            int random = Random.RandomRange(0, m_listItems.Count);
            if (!m_listValues.Contains(random))
            {
                m_listValues.Add(random);
                index++;
            }
           
        }
    }
    private void SetupItemInit()
    {
        m_valueItem1 = 0;
        m_valueItem2 = 0;
        m_valueItem3 = 0;
        for (int i=0;i< m_listValues.Count; i++)
        {
            GameObject item = m_listItems[i].transform.Find("icon").gameObject;
            SetupImageItem(item, i);
        }
    }
    private void SetupImageItem(GameObject item,int index)
    {
       
        Image img = m_listItems[index].transform.Find("icon").GetComponent<Image>();
        TextMeshProUGUI text = m_listItems[index].transform.Find("text/text-value").GetComponent<TextMeshProUGUI>();
        switch (index.ToString())
        {
            case "0": // amor
                img.sprite = m_listSprites[0];
                text.text = "+10 Amor";
                m_valueItem1 = 10;
                break;
            case "1": // critical chance
                img.sprite = m_listSprites[1];
                text.text = "+5 Critical Chance";
                m_valueItem2 = 5;
                break;
            case "2": // critical damage
                img.sprite = m_listSprites[2];
                text.text = "+25 Critical Damage";
                m_valueItem3 = 25;
                break;
        }
        //Debug.Log("PanelWinGame--1" + m_valueItem1);
        //Debug.Log("PanelWinGame--2" + m_valueItem2);
        //Debug.Log("PanelWinGame--3" + m_valueItem3);
    }
    private void OnActiveListItems(bool active)
    {
        m_bntGo.SetActive(true);
        foreach (var item in m_listItems)
            item.gameObject.SetActive(active);
        //
        if (active)
            m_bntGo.SetActive(false);
    }
    private void OnButtonWinGame(bool win = false)
    {
        m_buttonGoWin.SetActive(false);
        m_bntGo.SetActive(false);
        if (win)
            m_buttonGoWin.SetActive(true);
        else
            m_bntGo.SetActive(true);
    }
    public void Item1OnClick()
    {
        GameController.UpdateProperties(0, m_valueItem1);
        OnActiveListItems(false);
        m_bntGo.SetActive(true);
    }
    public void Item2OnClick()
    {
        GameController.UpdateProperties(1, m_valueItem2);
        OnActiveListItems(false);
        m_bntGo.SetActive(true);
    }
    public void ItemOn3Click()
    {
        GameController.UpdateProperties(2, m_valueItem3);
        OnActiveListItems(false);
        m_bntGo.SetActive(true);
    }
   
    public void ResetItemWeapon()
    {
        ToggleInventory(true);
    }
}
