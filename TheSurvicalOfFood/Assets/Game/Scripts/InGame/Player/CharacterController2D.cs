using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CharacterController2D : MonoBehaviour
{
    public Action<bool> rightMoveEvent;
    public Action moveEvent;
    public Action stopMoveEvent;

    [SerializeField] private int m_armor = 10;
    [SerializeField] private float m_criticalChance = 0.05f;
    [SerializeField] private float m_criticalDamage = 0.5f;
    [SerializeField] private int m_damage = 20;
    [SerializeField] private int m_maxHp = 100;
    [SerializeField] private int m_speedMovement = 3;

    [SerializeField] private CircleCollider2D m_circleCollider2d;
    [SerializeField] private Rigidbody2D m_rigidbody2D;
    [SerializeField] private GameObject m_camera;
    [SerializeField] private Joystick m_joystick;
    // Start is called before the first frame update
    Vector2 m_movement;
    private bool isActive = false;
    private void OnEnable()
    {
        m_camera.transform.position = transform.position;//new Vector3(0, 0, -10);
    }
    private void Awake()
    {
        GameController.activeGame += OnActiveGame;
    }
    private void OnDestroy()
    {
        GameController.activeGame -= OnActiveGame;
    }
    // Update is called once per frame
    void Update()
    {
        if (isActive)
        {
            //m_movement.x = m_joystick.inputHorizontal();//Input.GetAxisRaw("Horizontal");
            //m_movement.y = m_joystick.inputVertical();//Input.GetAxisRaw("Vertical");
            m_movement = m_joystick.GetInput();
           // m_camera.transform.position = Vector3.Lerp(m_circleCollider2d.transform.position,new Vector3(m_camera.transform.position.x, m_camera.transform.position.y , -10), 0.8f);
            //m_camera.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
        }
        else
        {
            m_movement = Vector2.zero;
        }
    }
    private void FixedUpdate()
    {
      if(isActive)
        {
            m_rigidbody2D.MovePosition(m_rigidbody2D.position + m_movement * m_speedMovement * Time.fixedDeltaTime);
           
            if (m_movement.x > 0)
                rightMoveEvent?.Invoke(true);
          
            else if (m_movement.x < 0)
                rightMoveEvent?.Invoke(false);
          


            if (m_movement.x == 0 && m_movement.y == 0)
            {
                GameController.UpdateMovement(false);
                stopMoveEvent?.Invoke();
            }
            else
            {
                GameController.UpdateMovement(true);
                moveEvent?.Invoke();
            }
        }

    }
    private void OnActiveGame(bool active)
    {
        isActive = active;
     
    }
}
