using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public WeaponType weaponType = WeaponType.Sword;
    public int damageWeapon = 50;
   // public bool isPos = true;
    public Transform target = null;


    private WeaponType m_currentWeaponType;
    public virtual void Awake()
    {
        m_currentWeaponType = weaponType;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void UpdateTargetEnemy(Transform objectBase)
    {
        if (objectBase == null)
        {
            target = null;
            //isPos = false;
        }
        else
            target = objectBase;
        //Debug.Log(m_target);
    }
    public int GetDamage()
    {
        return damageWeapon;
    }
    public Transform GetTarget()
    {
        return target;
    }
}
public enum WeaponType
{
    Sword,
    Bow,
    Gun
}

