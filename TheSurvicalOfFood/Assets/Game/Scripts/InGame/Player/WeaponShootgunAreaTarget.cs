using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponShootgunAreaTarget : MonoBehaviour
{
    [SerializeField] private WeaponShootgun m_weaponShootgun;

    private ObjectBase m_objectBase;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_ENEMY_1)
            return;
        m_objectBase = collider.GetComponent<ObjectBase>();
        if (m_objectBase == null)
            return;
        m_weaponShootgun.UpdateTargetEnemy(m_objectBase.transform);
    }
}
