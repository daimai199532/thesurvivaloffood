using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Joystick : MonoBehaviour, IDragHandler //, IPointerUpHandler, IPointerDownHandler
{
    [SerializeField] private GameObject m_imgJoystickBg; // joystickBG
    [SerializeField] private GameObject m_imgJoystick;   // joystick

    //public RectTransform m_positionStart;
    private Vector2 m_joystickTouchPos;
    private RectTransform m_joystickOriginalPos;


    private Vector2 m_posInput;
    private void OnEnable()
    {
        m_posInput =  Vector2.zero;
        m_imgJoystick.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.y * 3f / 2f);
        m_imgJoystickBg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.y * 3f / 2f);
    }
    private void OnDisable()
    {
        m_posInput = Vector2.zero;
    }
    private void Start()
    {
        m_joystickOriginalPos = m_imgJoystickBg.GetComponent<RectTransform>();
       // m_positionStart = GetComponent<RectTransform>();
       // m_positionStart.anchoredPosition = new Vector2(0, 0);
    }
   
    public void PointerDown()
    {
        //Debug.Log(Input.mousePosition);
        m_joystickTouchPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_imgJoystick.transform.position = new Vector3(m_joystickTouchPos.x, m_joystickTouchPos.y,0); //Camera.main.ScreenToWorldPoint(Input.mousePosition);
        m_imgJoystickBg.transform.position = new Vector3(m_joystickTouchPos.x, m_joystickTouchPos.y, 0);// Camera.main.ScreenToWorldPoint(Input.mousePosition);// Input.mousePosition;

    }
    public void PointerUp()
    {
        
        m_imgJoystick.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.y * 3f / 2f);
        m_imgJoystickBg.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.y *3f/2f);
        //
        m_posInput = Vector2.zero;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(m_joystickOriginalPos, eventData.position, eventData.pressEventCamera, out m_posInput))
        {
            m_posInput.x = m_posInput.x / (m_joystickOriginalPos.sizeDelta.x * .4f);
            m_posInput.y = m_posInput.y / (m_joystickOriginalPos.sizeDelta.y * .4f);
            if (m_posInput.magnitude > 1f)
            {
                m_posInput = m_posInput.normalized;
            }
        
            m_imgJoystick.GetComponent<RectTransform>().anchoredPosition = new Vector2(m_posInput.x * (m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.x / 3) + m_joystickOriginalPos.anchoredPosition.x, m_posInput.y * (m_imgJoystickBg.GetComponent<RectTransform>().sizeDelta.y / 3) + m_joystickOriginalPos.anchoredPosition.y);

        
        }
    }
    public Vector2 GetInput()
    {
        return m_posInput;
    }    
    public int inputHorizontal()
    {
        if (m_posInput.x < 0)
        {

            m_posInput.x = -1;
        }
        else if (m_posInput.x > 0)
            m_posInput.x = 1;
        else
            m_posInput.x = 0;
        return (int)m_posInput.x;

    }
    public int inputVertical()
    {
        if (m_posInput.y < 0)
            m_posInput.y = -1;
        else if (m_posInput.y > 0)
            m_posInput.y = 1;
        else
            m_posInput.y = 0;
        return (int)m_posInput.y;

    }
}
