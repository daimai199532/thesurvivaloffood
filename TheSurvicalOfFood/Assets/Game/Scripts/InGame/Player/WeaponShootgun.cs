using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using DG.Tweening;

public class WeaponShootgun : MonoBehaviour
{
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_bullet;
    private Transform m_target;
    private bool isAttack = true;
    private void OnEnable()
    {
        GameController.movementEvent += UpdateStatusMovement;
        StartCoroutine(WaitOnStart(0.5f));
        //m_spine.AnimationState.SetAnimation(0, "bow_attack", true);

    }
    private void OnDisable()
    {
        GameController.movementEvent -= UpdateStatusMovement;
        StopAllCoroutines();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_target != null)
        {
            var dir = m_target.transform.position - transform.position;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            ///Debug.Log(angle + "=====angle");
            if (angle < 90 && angle > -90)
                transform.localScale = new Vector3(1, 1, 1);
            else
            {
                angle = 180 + angle;
                transform.localScale = new Vector3(-1, 1, 1);
            }
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
        else
            transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
    }
    IEnumerator WaitOnStart(float time)
    {

        yield return new WaitForSeconds(time);
        isAttack = true;
    }
    public void UpdateTargetEnemy(Transform tranform)
    {
        m_target = tranform;
        OnAttack();
    }
    private void OnAttack()
    {
        if(isAttack)
        {
            if(m_target !=null)
            {
                StartCoroutine(WaitOnAttack(5f));
            }
        }
    }
    IEnumerator WaitOnAttack(float timeWait)
    {
        yield return null;
        
        TrackEntry trackEntry = m_spine.AnimationState.SetAnimation(0, "bow_attack", false);
        //Debug.Log(trackEntry.AnimationEnd)
        yield return new WaitForSeconds(trackEntry.AnimationEnd/2f);
        isAttack = false;
        //Debug.Log(trackEntry.AnimationEnd + "=================time");
        m_bullet.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        m_bullet.SetActive(false);
        yield return new WaitForSeconds(timeWait);
        isAttack = true;
    }
    private void UpdateStatusMovement(bool isRun)
    {
        //Debug.Log(isAttack + "==============");
        if(!isAttack)
        {
            if (isRun)
                m_spine.AnimationState.SetAnimation(0, "bow_run", true);
            else
                m_spine.AnimationState.SetAnimation(0, "bow_idle", true);
        }
    }
}
