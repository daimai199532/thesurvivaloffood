using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class WeaponSwordAreaTarget : MonoBehaviour
{
   // public static Action<Transform> updateTarget;

    [SerializeField] private WeaponSword m_weaponSword;
    [SerializeField] private CircleCollider2D m_circleCollider2D;

   // private Transform m_target;
    private ObjectBase m_target;
    private bool isCurrentTarget = false;

    private float m_distance = 0;
    private List<ObjectBase> _listObj = new List<ObjectBase>();
    private void OnEnable()
    {
        m_target = null;
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
       // Debug.Log(m_target);
        if (m_target != null)
        {
            if (m_target.isDead)
            {
                //Debug.Log(m_target.isDead + "=============");
                m_target = null;
                return;
            }

        }
           
        ObjectBase obj = collider.GetComponent<ObjectBase>();
        
       // bool dead = collider.GetComponent<ObjectBase>().isDead;
        if (obj == null || obj.isDead)
        {
            return;
        }
       
        float distance = Vector3.Distance(obj.transform.position, transform.position);
        if (distance > m_circleCollider2D.radius)
           return;
        m_target = obj;
        m_weaponSword.UpdateTargetEnemy(obj.transform);
    }
    private void Update()
    {
        if (m_target == null)
            return;
        float distance = Vector3.Distance(m_target.transform.position, transform.position);
        if (distance > m_circleCollider2D.radius || m_target.isDead)
        {
            m_target = null;
            m_weaponSword.UpdateTargetEnemy(null);
        }
    }

    //private void OnTriggerStay2D(Collider2D collider)
    //{
    //    if (collider.tag != GameConstant.TAG_ENEMY_1)
    //        //m_weaponBow.UpdateTargetEnemy(null);
    //        return;

    //    ObjectBase obj = collider.GetComponent<ObjectBase>();


    //    if (obj == null)
    //    {
    //        return;
    //    }

    //    float distance = Vector3.Distance(obj.transform.position, transform.position);
    //    //Debug.Log(boxCollider2d.radius + "-------------distance");
    //    if (distance> m_circleCollider2D.radius)
    //        return;
    //    m_weaponSword.UpdateTargetEnemy(obj.transform);
    //        //_listObj.Add(obj);
    //        //m_weaponSword.UpdateTargetEnemy(CheckDistance().transform);
    //}
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (m_target != collider.transform)
            return;
        m_target = null;
        m_weaponSword.UpdateTargetEnemy(null);
    }
    ObjectBase CheckDistance()
    {
        ObjectBase temp = _listObj[0];
        if (_listObj.Count > 3)
        {
            float numberMin = Vector3.Distance(_listObj[0].transform.position, transform.position);
            float numberZero = 0;
            int numberCount = 0;
            for (int i = 1; i < _listObj.Count - 1; i++)
            {
                //int j = i + 1;
                numberZero = Vector3.Distance(_listObj[i].transform.position, transform.position);
                if (numberMin > numberZero &&  numberZero < m_circleCollider2D.radius)
                {
                    numberMin = numberZero;
                    numberCount = i;
                    temp = _listObj[numberCount];
                }
            }
            _listObj = new List<ObjectBase>();
            return temp;
        }
        _listObj = new List<ObjectBase>();
        return temp;

    }
}
