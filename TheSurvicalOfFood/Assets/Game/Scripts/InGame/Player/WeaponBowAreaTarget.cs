using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBowAreaTarget : MonoBehaviour
{
    [SerializeField] private WeaponBow m_weaponBow;

    private ObjectBase m_objectBase;
    //private Transform m_target;
    private bool isTarget = false;
    private List<ObjectBase> _listObj = new List<ObjectBase>();
    private void OnTriggerStay2D(Collider2D collider)
    {
       
        if (collider.tag != GameConstant.TAG_ENEMY_1)
            //m_weaponBow.UpdateTargetEnemy(null);
             return;

        ObjectBase obj = collider.GetComponent<ObjectBase>();
        if (obj == null)
        {
            
            return;
        }
        else
        {
            _listObj.Add(obj);
            // m_weaponBow.UpdateTargetEnemy(obj.transform);
            m_weaponBow.UpdateTargetEnemy(CheckDistance().transform);
        }
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_ENEMY_1)
            return;
        m_weaponBow.UpdateTargetEnemy(null);
    }
    ObjectBase CheckDistance()
    {
        ObjectBase temp = _listObj[0];
        if (_listObj.Count > 1)
        {
            float SNN = Vector3.Distance(_listObj[0].transform.position, transform.position);
            float SNN1 = 0;
            int STT = 0;
            for (int i = 1; i < _listObj.Count - 1; i++)
            {
                //int j = i + 1;
                SNN1 = Vector3.Distance(_listObj[i].transform.position, transform.position);
                if (SNN > SNN1)
                {
                    SNN = SNN1;
                    STT = i;
                    temp = _listObj[STT];
                }
            }
            _listObj = new List<ObjectBase>();
            return temp;
        }
        _listObj = new List<ObjectBase>();
        return temp;

    }
    IEnumerator WaitOnCheckDistance()
    {
        yield return new WaitForSeconds(0.5f);
    }
}
