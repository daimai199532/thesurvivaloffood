using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using Spine;
using Spine.Unity;

public class WeaponSword : Weapon
{
    
  
   
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_boxCollider2D;
    [SerializeField] private float m_distance = 0.5f;
     private bool isAttack = true;
   
    private void OnEnable()
    {
        
        GameController.movementEvent += UpdateAnim;
        m_boxCollider2D.SetActive(false);
        StartCoroutine(WaitOnComplete(0.5f));
    }
    private void OnDisable()
    {
        GameController.movementEvent -= UpdateAnim;
        StopAllCoroutines();
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        OnRotation(base.GetTarget(),transform);
    }
   
    private void OnRotation(Transform target, Transform current)
    {
        //Debug.Log(target != null);
        float angle = 0;
        if (target != null)
        {
            var dir = target.transform.position - current.transform.position;
            angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            m_spine.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            OnAttack();
        }
        else
             m_spine.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    
    private void OnAttack()
    {

        if (isAttack)
        {

            if (base.GetTarget() != null)
            {

                float distanceAB = Vector3.Distance(transform.position, base.GetTarget().transform.position);
                
                if (m_distance > distanceAB)
                {
                    m_boxCollider2D.SetActive(true);
                    isAttack = false;
                   // isPos = false;
                    StartCoroutine(WaitOnComplete(3f));
                    var dir = base.GetTarget().transform.position - transform.position;

                    m_spine.transform.DOMove(base.GetTarget().transform.position, 0.1f).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        m_boxCollider2D.SetActive(false);
                        m_spine.transform.DOLocalMove(new Vector3(0, 0, 0), 0.1f).SetEase(Ease.Linear).OnComplete(() =>
                        {
                           // isPos = true;
                            base.target = null;
                            m_boxCollider2D.SetActive(false);
                            //GameController.UpdateTargetWeapon();
                        });
                    });
                }
                else
                {
                    base.target = null;
                }

            }
        }
       

    }
    IEnumerator WaitOnComplete(float time)
    {
        yield return new WaitForSecondsRealtime(time);
        isAttack = true;
       // StopAllCoroutines(); 
    }
   
    private void UpdateAnim(bool isRun)
    {
        if (isRun)
            m_spine.AnimationState.SetAnimation(0, "sword_run", true);
        else
            m_spine.AnimationState.SetAnimation(0, "sword_idle", true);
    }
  
}
