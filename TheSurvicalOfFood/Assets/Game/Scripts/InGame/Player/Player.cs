using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using TMPro;
using DG.Tweening;

public class Player : MonoBehaviour
{
    private const float MAX_WIDTH_PROGRESS = 0.78f;
    [SerializeField] private CharacterController2D m_characterController2D;
 
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private SpriteRenderer m_healthProcess;
    [SerializeField] private TextMeshPro m_levelStar;
    [SerializeField] private GameObject m_objLootItem;
    [SerializeField] private GameObject m_effectLootItem;
    [SerializeField] private GameObject m_textHurt;
    [SerializeField] private CircleCollider2D m_circleCollider;
    [SerializeField] private ParticleSystem m_effectDead;
    [SerializeField] private GameObject m_weapon;
    [SerializeField] private GameObject m_AllHealthProcess;
    //public int m_health = 100;
    private bool isMove = false;
    private bool isDead = false;

    private void OnEnable()
    {
        Application.targetFrameRate = 60;
        m_characterController2D.moveEvent += OnMove;
        m_characterController2D.stopMoveEvent += StopMove;
        m_characterController2D.rightMoveEvent += UpdateMoveRight;
        GameController.updateHealthEvent += OnHurt;
        //GameController.lootItemEvent += OnLootItem;
        //GameController.restartGame += Init;
        GameController.finishEvent += OnFinish;
      
        Init();
       
    }
    private void OnDisable()
    {
        GameController.updateHealthEvent -= OnHurt;
        m_characterController2D.moveEvent -= OnMove;
        m_characterController2D.stopMoveEvent -= StopMove;
        m_characterController2D.rightMoveEvent -= UpdateMoveRight;
        //GameController.lootItemEvent -= OnLootItem;
        //GameController.restartGame -= Init;
        GameController.finishEvent -= OnFinish;
       
    }
    private void Start()
    {
        
    }
    // Start is called before the first frame update
    private void Init()
    {
        //transform.position = Vector3.zero;
        m_circleCollider.enabled = false;
        isDead = false;
        //
        m_levelStar.text = MainModel.m_currentStar.ToString();
        m_objLootItem.SetActive(false);
        
        m_circleCollider.enabled = true;
        m_AllHealthProcess.SetActive(true);
        m_weapon.SetActive(true);
        m_spine.gameObject.SetActive(true);

    }
    private void OnMove()
    {
        
        StartCoroutine(WaitOnMove());
    }
    IEnumerator WaitOnMove()
    {
        //Debug.Log(isMove);
        yield return null;
        if(isMove)
        {
            isMove = false;
            TrackEntry entry = m_spine.AnimationState.SetAnimation(0, GameConstant.ANIM_PLAYER_RUN, true);
            yield return new WaitForSeconds(entry.AnimationEnd );
            isMove = true;
        }
    }
    private void StopMove()
    {
        StartCoroutine(WaitStopMove());
    }
    IEnumerator WaitStopMove()
    {
        yield return null;
        if (!isMove)
        {
            isMove = true;
            TrackEntry entry = m_spine.AnimationState.SetAnimation(0, GameConstant.ANIM_PLAYER_IDLE_INGAME, true);
            m_spine.AnimationState.SetAnimation(0, GameConstant.ANIM_PLAYER_IDLE_INGAME, true);
            yield return new WaitForSeconds(entry.AnimationEnd);
            isMove = false;
        }
        //yield return null;
        //StopAllCoroutines();
    }
   
    public void UpdateMoveRight(bool isRight)
    {
        if (isRight)
        {
            m_spine.transform.localScale = new Vector3(1, 1, 1);
            
        }
        else
        {
            m_spine.transform.localScale = new Vector3(-1, 1, 1);
           
        }
           
    }
    private void OnHurt(int Health, bool isHealth)
    {
       
        if (isHealth && Health < 1)
            return;
        
        OnText(Health);
        Vector2 size = m_healthProcess.size;
        float oldVal = size.x;
        float newVal = MAX_WIDTH_PROGRESS * MainModel.m_currentHealth / MainModel.m_maxHealth;
        if (Health == 0)
        {
            m_healthProcess.size = new Vector2(newVal, size.y);
            return;
        }
        else
            OnText(Health);
        m_healthProcess.size = new Vector2(newVal, size.y);
        if (MainModel.m_currentHealth < 1)
            Dead();
        //Debug.Log(MainModel.m_currentHealth + "====" + MainModel.m_maxHealth);
    }
    private void OnText(int valueDamage)
    {
        //Debug.Log(valueDamage);
        GameObject m_text = Instantiate(m_textHurt);
        TextMeshPro text = m_text.GetComponent<TextMeshPro>();

        if (valueDamage < 0)
        {
            text.text =  valueDamage.ToString();
            text.color = Color.red;
        }
        if (valueDamage > 0)
        {
            text.color = Color.green;
            text.text = "+" +valueDamage.ToString();
        }
                
        m_text.transform.position = m_textHurt.transform.position;
        m_text.SetActive(true);
        m_text.transform.SetParent(transform.parent, true);
        m_text.transform.localScale = new Vector3(1, 1, 1);// new Vector3(transform.localScale.x, 1, 1);
        //m_text.transform.localEulerAngles = Vector3.zero;
        m_text.transform.DOMoveY(m_text.transform.position.y + .5f, 0.2f).OnComplete(() => {
            text.DOFade(0, 0.3f).OnComplete(() => {
                m_text.transform.DOKill();
                text.DOKill();
                Destroy(m_text);
            });
        });
    }
    private void Dead()
    {
        isMove = false;
        GameController.StopTimeCountDown();
        StartCoroutine(WaitDead());
    }
    IEnumerator WaitDead()
    {
        //Debug.Log("dead===");
        m_AllHealthProcess.SetActive(false);
        m_weapon.SetActive(false);
        m_spine.gameObject.SetActive(false);
        m_circleCollider.enabled = false;
        GameObject effect = Instantiate(m_effectDead.gameObject);
        effect.transform.position = transform.position;
        effect.transform.SetParent(transform.parent);
        effect.SetActive(true);
        yield return new WaitForSeconds(2f);
        //yield return null;
        Destroy(effect);
        transform.DOScale(1, 1f).OnComplete(GameController.FailLevel);
       
    }
    private void OnLootItem()
    {
        StartCoroutine(WaitLootItem());
    }
    IEnumerator WaitLootItem()
    {
        m_objLootItem.SetActive(true);
        m_circleCollider.enabled = true;
        yield return new WaitForSeconds(3f);
        m_objLootItem.SetActive(false);
        m_circleCollider.enabled = false;
        //GameController.OnFinishGame();
    }
    private void OnFinish()// WinComplete
    {
        m_spine.AnimationState.SetAnimation(0, GameConstant.ANIM_PLAYER_IDLE_INGAME, true);
        m_circleCollider.enabled = false;
        OnLootItem();
        transform.DOScale(1, 5f).OnComplete(OnExit);
       
    }
    private void OnExit()
    {
        GameController.CompleteLevel();
    }
    //private void OnTriggerStay2D(Collider2D collider)
    //{
    //    if(collider.tag == "Wall")
    //    {
    //        Debug.Log("On wall");
    //    }
    //}
}
