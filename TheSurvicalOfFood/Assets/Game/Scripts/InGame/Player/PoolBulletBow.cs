using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class PoolBulletBow : MonoBehaviour
{
    public static PoolBulletBow instance;
    //
    [SerializeField] private Player m_player;
    [SerializeField] private GameObject m_content;
    [SerializeField] private GameObject bulletPrefabs;
    [SerializeField] private GameObject m_objMove;
   // [SerializeField] private List<WeaponBow> m_listWeapons;
    [SerializeField] private List<Weapon> m_listWeapons;
    [SerializeField] private List<ObjectBase> m_listTargets;
    //[SerializeField] private List<GameObject> m_listWeaponsSword;
    private int amountToPool = 10;
    private List<GameObject> pooledObject = new List<GameObject>();
    
    private void OnEnable()
    {
        GameController.activeGame += ActiveBullet;
        GameController.targetWeaponEvent += SetTargetWeapon;
    }
    private void OnDisable()
    {
        GameController.activeGame -= ActiveBullet;
        GameController.targetWeaponEvent -= SetTargetWeapon;
    }
    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    private void Start()
    {
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(bulletPrefabs);
            obj.transform.SetParent(m_content.transform, false);
            obj.SetActive(false);
            pooledObject.Add(obj);
        }
        SetTargetWeapon();
        
    }
    private void Update()
    {
        SetTargetWeapon();
        //if (m_listTargets.Count > 1)
        //    Debug.Log(m_listTargets.Count);
    }
    public GameObject GetPooledObject()
    {
        for (int i = 0; i < pooledObject.Count; i++)
        {
            if (!pooledObject[i].activeInHierarchy)
            {
                return pooledObject[i];
            }
        }
        return null;
    }
    private void ActiveBullet(bool isActive)
    {
        if(!isActive)
             foreach (var item in pooledObject)
                 item.SetActive(false);
    }
  
    private void SetTargetWeapon()
    {
       ObjectBase obj = FindCloseEnemy();
        int count = 0;
      if(obj !=null)
        foreach (Weapon item in m_listWeapons)// foreach (WeaponBow item in m_listWeapons)
        {
           // Debug.Log(m_listTargets.Count + "====" + m_listWeapons.Count);
            if (item.GetTarget() == null) // check target current
            {
                if (obj == null)
                     return;
                    count++;
                //Debug.Log(obj.health + "====" );
                if (obj.health < item.GetDamage()) // check health
                {
                    item.UpdateTargetEnemy(obj.transform);
                    m_listTargets.Remove(obj);
                    obj = FindCloseEnemy();
                }
                else
                {
                    item.UpdateTargetEnemy(obj.transform);
                }
            }
        }
    }
    private ObjectBase FindCloseEnemy()
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        ObjectBase closestEnemy = null;
        if(m_listTargets.Count > 0)
        {
            foreach (ObjectBase obj in m_listTargets)
            {
                float distanceToEnemy = (obj.transform.position - m_player.transform.position).sqrMagnitude;
                if (distanceToEnemy < distanceToClosestEnemy)
                {
                    distanceToClosestEnemy = distanceToEnemy;
                    closestEnemy = obj;
                }
            }
        }
        //Debug.DrawLine(transform.position, closestEnemy.transform.position, Color.red);
        return closestEnemy;

    }

    private void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_ENEMY_1)
            return;
        ObjectBase obj = collider.GetComponent<ObjectBase>();
        if (obj == null)
            return;
        //Debug.Log(obj.isDead);
        if(obj.isDead)
        {
            if (m_listTargets.Contains(obj))
                m_listTargets.Remove(obj);
        }
        else
            if (!m_listTargets.Contains(obj))
                   m_listTargets.Add(obj);
       
    }
    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_ENEMY_1)
            return;
        ObjectBase obj = collider.GetComponent<ObjectBase>();
        m_listTargets.Remove(obj);
    }
}
