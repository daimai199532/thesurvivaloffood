using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine;
using Spine.Unity;

public class WeaponBow : Weapon
{
   
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_bullet;
    

    private bool m_isAttack = true;
   

    private void OnEnable()
    {
    
    }
    private void OnDisable()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (base.GetTarget() != null)
        {

            Vector2 direction = base.GetTarget().transform.position - transform.position;
            float angle = Mathf.Rad2Deg * Mathf.Atan2(direction.y, direction.x);
            transform.eulerAngles = new Vector3(0, 0, angle);
            Debug.DrawLine(transform.position, base.GetTarget().transform.position, Color.red);
        }
        else
        {
            transform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
        }
        if (base.GetTarget() != null)
        {
            OnAttack();
        }
        else
        {
            //isPos = true;
        }
    }
   
    private void OnAttack()
    {
       // Debug.Log(isAttack + "=============" + (m_target != null));
        if (m_isAttack)
        {
            if(base.GetTarget() != null)
            {
              
                //isPos = false;
                m_isAttack = false;
                //Transform target = m_target.transform;
                Vector2 direction = (base.GetTarget().transform.position - transform.position);
                float angle =  Mathf.Rad2Deg * Mathf.Atan2(direction.y, direction.x);
                //
                TrackEntry trackEntry = m_spine.AnimationState.SetAnimation(0, "bow_attack", false);
                //
                GameObject bullet = PoolBulletBow.instance.GetPooledObject();// ObjectPooling.instance.GetPooledObject();//
                if (bullet != null)
                
                {
                  
                    bullet.transform.position = m_bullet.transform.position;
                    transform.eulerAngles = new Vector3(0, 0, angle);
                    bullet.transform.eulerAngles = new Vector3(0, 0, angle);
                    //
                    Sequence seq = DOTween.Sequence();

                    seq.SetDelay(trackEntry.AnimationEnd / 20f);
                   
                    bullet.SetActive(true);
                  
                    seq.Append(bullet.transform.DOMove((Vector2)transform.position + direction.normalized * 10f, 1f).OnComplete(() =>
                    {
                        //isPos = true;
                        bullet.gameObject.SetActive(false);
                        //Destroy(bullet, 0.1f);
                    }));
                    seq.AppendInterval(1f).OnComplete(() =>
                    { // delay 1f sau khi tan cong
                        m_isAttack = true;
                        base.target= null;
                        //GameController.UpdateTargetWeapon();
                    });
                }
            }
        }
        else
        {
          //  isPos = true;
        }
    }
   
}
