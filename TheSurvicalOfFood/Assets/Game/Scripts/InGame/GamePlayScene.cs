using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class GamePlayScene : MonoBehaviour
{
    [SerializeField] private GameObject m_panelFinishGame;
    [SerializeField] private Slider m_sliderHealthProcess;
    [SerializeField] private TextMeshProUGUI m_textHealthPlayer;
    [SerializeField] private TextMeshProUGUI m_textTimeCountDown;
    [SerializeField] private Image m_iconTimeCountDown;
    [SerializeField] private TextMeshProUGUI m_textMapLevel;
    [SerializeField] private TextMeshProUGUI m_textCoin;
    [SerializeField] private Slider m_sliderStarProcess;
    [SerializeField] private TextMeshProUGUI m_textLevelStar;
    [SerializeField] private TextMeshProUGUI m_textExpLevelStar;
    [SerializeField] private GameObject m_panelAnimStart;
    [SerializeField] private GameObject m_player;
    [SerializeField] private GameObject m_objJoystick;
    private IEnumerator m_timeCountDown;
    [SerializeField] private GameObject m_panelStart;
    [SerializeField] private GameObject m_panelRevive;

    private Coroutine m_timeCount;
    private Coroutine m_timeCountRevive;
    // Start is called before the first frame update
    private void Awake()
    {
        m_panelStart.SetActive(true);
        MainModel.LoadDataUser();
        GameController.updateHealthEvent += OnUpdateHealth;
        GameController.updateCoinEvent += OnUpdateCoin;
        //GameController.updateExpStarLevelEvent += OnUpdateStar;
        GameController.updateMapLevelEvent += OnUpdateMapLevel;
        GameController.finishGamePlaySceneEvent += OnFisinhGame;
        GameController.stopTimeCountDown += StopTimeCountDown;
       
    }
    private void OnDisable()
    {
        GameController.updateHealthEvent -= OnUpdateHealth;
        GameController.updateCoinEvent -= OnUpdateCoin;
        //GameController.updateExpStarLevelEvent -= OnUpdateStar;
        GameController.updateMapLevelEvent -= OnUpdateMapLevel;
        GameController.finishGamePlaySceneEvent -= OnFisinhGame;
        GameController.stopTimeCountDown -= StopTimeCountDown;
       
    }
    void Start()
    {
      
    }

    // Update is called once per frame
    private void Init()
    {
        m_objJoystick.SetActive(false);
        m_textTimeCountDown.text = "60";
        //
        GameController.UpdateLevelCoin(0);
        GameController.UpdateHealth(0);
        GameController.UpdateExpLevelStar(0);
        //GameController.OnActiveGame(false);
        StartCoroutine(OnInitComplete());
        
    }
    IEnumerator OnInitComplete()
    {
        yield return null;
        GameObject objReady = m_panelAnimStart.transform.Find("ready").gameObject;
        RectTransform objGo = m_panelAnimStart.transform.Find("go").GetComponent<RectTransform>();
        //
        m_panelAnimStart.SetActive(true);
        m_objJoystick.SetActive(true);
        //yield return new WaitForSeconds(1.5f);
        m_iconTimeCountDown.fillAmount = 1;
        //
        objGo.GetComponent<RectTransform>().anchoredPosition = new Vector2(400, 0);
        objGo.GetComponent<Image>().DOFade(1, 0.01f);

        objReady.GetComponent<Image>().DOFade(1, 0.01f);
        objReady.transform.localScale = new Vector2(3, 3);
        objReady.SetActive(true);
        objReady.transform.DOScale(1f, 0.5f).OnStepComplete(() => {
                objGo.gameObject.SetActive(true);
                objGo.DOAnchorPosX(0, 0.5f).OnComplete(() => {
                objGo.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(() => { objGo.gameObject.SetActive(false); });
            });
        }).OnComplete(()=> {
            objReady.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(() => {
            });

        });

       
        yield return new WaitForSecondsRealtime(2f);
        GameController.OnActiveGame(true);
        m_timeCount = StartCoroutine(TimeCountDown());
    }
    IEnumerator TimeCountDown()
    {
        yield return null;
        int timeMax = 10;
        int timeCurrent = 10;
        
    
        while (timeCurrent > -1)
        {
            m_iconTimeCountDown.DOFillAmount((float)timeCurrent / timeMax, 1.5f);
            // m_iconTimeCountDown.fillAmount = (float)timeCurrent / timeMax;
            m_textTimeCountDown.text = timeCurrent.ToString();
            timeCurrent--;
            yield return new WaitForSeconds(1f);
        }

        //GameController.CompleteLevel();  // win -> off obj -> playerAnim (clean obj-coin) -> panel complete
        GameController.OnActiveGame(false);
        GameController.Finish();
        GameController.StopTimeCountDown();
     
    }
   private void  StopTimeCountDown()
    {
        if(m_timeCount != null)
        StopCoroutine(m_timeCount);
        OnActiveJoyStick();
    }
    private void OnActiveJoyStick()
    {
        
     m_objJoystick.SetActive(false);
      
    }
    private void OnUpdateHealth(int health, bool isHeartMax)
    {
       // Debug.Log(MainModel.m_currentHealth + "===========" + MainModel.m_maxHealth);
        m_sliderHealthProcess.value = (((float)MainModel.m_currentHealth / MainModel.m_maxHealth));
        m_textHealthPlayer.text = (MainModel.m_currentHealth + "/" + MainModel.m_maxHealth).ToString();
    }
    private void OnUpdateCoin(int coin)
    {
        m_textCoin.text = (MainModel.m_totalCoin + MainModel.levelCoin).ToString();
    }
    private void OnUpdateStar(int start)
    {
        //Debug.Log(MainModel.levelStar + "====" + MainModel.m_currentExpStar);
        m_sliderStarProcess.value = ((float)MainModel.levelStar / 10);
        m_textExpLevelStar.text = MainModel.m_currentExpStar.ToString();
        m_textLevelStar.text = MainModel.m_currentStar.ToString();
    }
    private void OnUpdateMapLevel(int level)
    {
        m_textMapLevel.text = "Day " + (MainModel.m_totalDay + MainModel.levelDay).ToString();
    }

    private void OnFisinhGame(bool isWin)
    {

       // Debug.Log("run================");
      
        m_objJoystick.SetActive(false);
        StartCoroutine(WaitFinishGame(isWin));

    }
    IEnumerator WaitFinishGame(bool isWin)
    {
        yield return null;
        GameObject btnNextLevel = m_panelFinishGame.transform.Find("btn-next-level").gameObject;
        //GameObject btnNewGame = m_panelFinishGame.transform.Find("btn-new-game").gameObject;
        TextMeshProUGUI m_textTile = m_panelFinishGame.transform.Find("text-tile").GetComponent<TextMeshProUGUI>();
        m_player.SetActive(false);
        if (isWin)
        {
            //Init();
           // btnNextLevel.SetActive(true);
           // btnNewGame.SetActive(false);
            m_textTile.text = "Win game";
            m_panelFinishGame.GetComponent<PanelWinGame>().Init(true);
            m_panelFinishGame.SetActive(true);
            GameController.ReStartGame();
            GameController.OnActiveGame(false);
            //StopAllCoroutines();
        }
        else // fail level == playerAnim -> panel revive -> off obj -> panel finish
        {
            m_panelFinishGame.SetActive(false);
            StartCoroutine(OnRevive());
           
        }
        
     
    }
    IEnumerator  OnRevive()
    {
        yield return null;
       
        GameController.ReStartGame();
        GameController.OnActiveGame(false);
        m_timeCountRevive =  StartCoroutine(TimeCountDownRevive());
      
    }
    IEnumerator TimeCountDownRevive()
    {
        yield return null;
        TextMeshProUGUI textTime = m_panelRevive.transform.Find("icon-oclock/time-value").GetComponent<TextMeshProUGUI>();
        Image imgProcess = m_panelRevive.transform.Find("icon-oclock/process").GetComponent<Image>();

        int timeMax = 10;
        int timeCurrent = 10;
        textTime.text = timeCurrent.ToString();
        imgProcess.fillAmount = 1;
        
        yield return new WaitForSeconds(0.01f);
        m_panelRevive.SetActive(true);
        //m_iconTimeCountDown.DOFillAmount(0, 69f);
        while (timeCurrent > - 0.5)
        {
            imgProcess.DOFillAmount((float)timeCurrent / timeMax, 2f);
            // m_iconTimeCountDown.fillAmount = (float)timeCurrent / timeMax;
            textTime.text = timeCurrent.ToString();
            timeCurrent--;
            yield return new WaitForSeconds(1f);
        }
        m_panelRevive.SetActive(false);
        m_panelStart.SetActive(true);
    }
    public void NextLevelOnClick()
    {
        StopAllCoroutines();
        // Debug.Log("[DEBUG] - NextLevelOnClick - finishGamePlaySceneEvent");
        //GameController.finishGamePlaySceneEvent += OnFisinhGame;
        m_player.SetActive(true);
        m_panelFinishGame.SetActive(false);
        //Time.timeScale = 1;
        MainModel.InitGameInfo();
        Init();
    }
   
    public void StartOnClick()
    {
        StopAllCoroutines();
        // Debug.Log("[DEBUG] - StartOnClick - finishGamePlaySceneEvent");
        //GameController.finishGamePlaySceneEvent += OnFisinhGame;
        m_player.SetActive(true);
        //Time.timeScale = 1;
        m_panelStart.SetActive(false);
        MainModel.InitGameInfo();
        Init();
        GameController.StartNemGame();
    }
    public void WinOnClick()
    {
        GameController.CompleteLevel();
        
    }
    public void LoseOnClick()
    {
        GameController.FailLevel();

    }
    public void ReviveOnClick()
    {
        // Revive , 50% health
        StopCoroutine(m_timeCountRevive);
        StopAllCoroutines();
        GameController.UpdateHealth(50);
        m_player.SetActive(true);
        //Time.timeScale = 1;
        //m_panelStart.SetActive(false);
        m_panelRevive.SetActive(false);
        MainModel.InitGameInfo();
        Init();
        GameController.StartNemGame();
    }
}
