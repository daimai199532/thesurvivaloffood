using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBase : MonoBehaviour
{
    public int health = 50;

    private int m_currentHealth ;


    private bool m_isdead = false;
    public virtual  void OnEnable()
    {
        health = 50;
        m_currentHealth = health;
        m_isdead = false;
        //Debug.Log(m_currentHealth + "==========base");
    }
    public bool isDead
    {
        get
        {
            return m_currentHealth < 1;
        }
    }
    public void Dead(bool isDead)
    {
        m_isdead = isDead;
    }
   
    protected virtual void Hurt(int damage)
    {
        m_currentHealth -= damage;
        if (m_currentHealth < 1)
            m_currentHealth = 0;

    }

}
