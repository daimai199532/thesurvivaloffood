using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using DG.Tweening;
using TMPro;

public class ObjectFly : ObjectBase
{
    [SerializeField] private BoxCollider2D m_boxCollider2d;
    [SerializeField] private ParticleSystem m_effectStart;
    [SerializeField] private ParticleSystem m_effectStart1;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_textDamageValue;
    [SerializeField] private GameObject m_prefabsBullet;
    [SerializeField] private GameObject m_objCoin;

    private int m_damagePlayer = 50;
    private float m_health = 50;
    private Player m_target;
   // private Transform m_target;
    private Vector2 m_curent = Vector2.zero;
    private bool isAttack;
    //private bool isDead;
    
    private bool isRun = false;
    //private void OnEnable()
    //{
       
    //    m_spine.AnimationState.SetAnimation(0, "idle", true) ;
       
    //}
   
    private void OnDisable()
    {
        isAttack = false;
        StopAllCoroutines();
    }
    private void OnDestroy()
    {
        StopAllCoroutines();
    }
    // Start is called before the first frame update
    void Start()
    {
       
        m_target = GameObject.FindGameObjectWithTag(GameConstant.TAG_PLAYER).GetComponent<Player>();

        //m_health = base.health;
       // Debug.Log(m_health);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_target != null && isRun)
        {
            transform.position = Vector2.SmoothDamp(transform.position, m_target.transform.position, ref m_curent, 10f, 2f);
            OnAttack();
            if (transform.position.x - m_target.transform.position.x < 0)
                transform.localScale = new Vector3(-1, 1, 1);
            else
                transform.localScale = new Vector3(1, 1, 1);
        }
        else
            transform.localScale = new Vector3(-1, 1, 1);

    }
    public void Init()
    {
        m_health = base.health;
        isRun = false;
        isAttack=false;
        //isDead=false;
        m_boxCollider2d.enabled = false;
        m_spine.gameObject.SetActive(false);
        m_effectStart1.gameObject.SetActive(false);
        m_effectStart1.gameObject.SetActive(false);
        StartCoroutine(WaitOnStart());
    }
    IEnumerator WaitOnStart()
    {
        yield return null;
        m_effectStart1.transform.position = transform.position;
        m_effectStart.transform.position = transform.position;
        m_effectStart.gameObject.SetActive(true);
        m_effectStart.Play();
        //
        //yield return new WaitForSeconds(2f);
        yield return new WaitForSeconds(1.5f);
        m_effectStart1.gameObject.SetActive(true);
        m_effectStart1.Play();
        //
        m_spine.gameObject.SetActive(true);
        // yield return new WaitForSeconds(1f);
        yield return new WaitForSeconds(0.5f);
        m_boxCollider2d.enabled = true;

        isRun = true;
        isAttack = true;
        //isDead = false;
        //m_target = null;
    }
    private void OnAttack()
    {
      //  Debug.Log(isAttack + " OnAttack");
        
        if (isAttack)
        {
           
            if (m_target != null)
            {
                isAttack = false;
              
                StartCoroutine(OnAnimAttack());
                GameObject bullet = Instantiate(m_prefabsBullet);
                bullet.transform.position = transform.position;
                //bullet.transform.localScale = Vector3.one;
                bullet.transform.SetParent(transform.parent, false);
                bullet.gameObject.SetActive(true);
                bullet.GetComponent<ObjectBulletEnemy>().Init(m_target.transform);
                //
                OnAttackComplete();

            }
        }
      
    }
    private void OnAttackComplete()
    {
      
        StartCoroutine(WaitOnAttackComplete(6f));
    }
    IEnumerator WaitOnAttackComplete(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        isAttack = true;
    }
    IEnumerator OnAnimAttack()
    {
        yield return null;
        TrackEntry trackEntry = m_spine.AnimationState.SetAnimation(0, "attack", false);
        yield return new WaitForSeconds(trackEntry.AnimationEnd);
        yield return new WaitForSeconds(1f);
        m_spine.AnimationState.SetAnimation(0, "idle", true);
    }
   
    IEnumerator Dead()
    {
        GameController.UpdateExpLevelStar(1);
        isAttack = false;
        //isDead = true;
        yield return null;
        TrackEntry trackEntry = m_spine.AnimationState.SetAnimation(0, "dead", false);
        //
        GameObject coin = Instantiate(m_objCoin, transform.parent);
        coin.transform.position = transform.position;
        coin.gameObject.SetActive(true);
        //
        yield return new WaitForSeconds(trackEntry.AnimationEnd);
        //yield return new WaitForSeconds(1f);
        StopAllCoroutines();
        gameObject.SetActive(false);
        
    }
   
   
    private void OnHurt(int valueDamage)
    {
        OnText(valueDamage);
        if (valueDamage - 1 > m_health)
            StartCoroutine(Dead());
        m_health = m_health - valueDamage;
    }
    private void OnText(int valueDamage)
    {
        GameObject m_text = Instantiate(m_textDamageValue);
        TextMeshPro text = m_text.GetComponent<TextMeshPro>();
        text.text = valueDamage.ToString();
        m_text.transform.position = m_textDamageValue.transform.position;
        m_text.SetActive(true);
        m_text.transform.SetParent(transform.parent, true);
        m_text.transform.localScale = new Vector3(1, 1, 1);// new Vector3(transform.localScale.x, 1, 1);
        m_text.transform.DOMoveY(m_text.transform.position.y + .3f, 0.5f).OnComplete(() => {
            text.DOFade(0, 0.5f).OnComplete(() => {
                m_text.transform.DOKill();
                text.DOKill();
                Destroy(m_text);
            });
        });
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (isDead)
            return;
        if (collider.tag == GameConstant.TAG_OBJECT_BULLET)
        {
            OnHurt(MainModel.damagePlayer);
            base.Hurt(MainModel.damagePlayer);
            
        }
        if(collider.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-m_damagePlayer, false);
        }
    }
}
