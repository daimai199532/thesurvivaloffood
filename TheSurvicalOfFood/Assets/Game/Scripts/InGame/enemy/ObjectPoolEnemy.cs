using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolEnemy : MonoBehaviour
{
    [SerializeField] private GameObject m_content;
    //
    public static ObjectPoolEnemy instance;

    private List<GameObject> pooledObjectWorm = new List<GameObject>();
    private int amountWormToPool = 100;
    [SerializeField] private GameObject wormPrefabs;
    //
    private List<GameObject> pooledObjectFly = new List<GameObject>();
    private int amountFlyToPool = 30;
    [SerializeField] private GameObject flyPrefabs;
    //
    private List<GameObject> pooledObjectBulletEnemy = new List<GameObject>();
    private int amountBulletEnemyToPool = 50;
    [SerializeField] private GameObject bulletEnemyPrefabs;
    //
    private List<GameObject> pooledObjectCoin = new List<GameObject>();
    private int amountBulletCoin = 100;
    [SerializeField] private GameObject coinPrefabs;
    //
    private List<GameObject> pooledObjectEffect = new List<GameObject>();
    private int amountEffect = 20;
    [SerializeField] private GameObject effectPrefabs;
    //

    private bool isActive = false;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        GameController.activeGame += OnActiveGame;
        //GameController.finishEvent += ResetGameObject;
        GameController.restartGame += ResetGameObject;
    }
    private void OnDestroy()
    {
        GameController.activeGame -= OnActiveGame;
        //GameController.finishEvent -= ResetGameObject;
        GameController.restartGame -= ResetGameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        for(int i=0; i< amountWormToPool; i++)
        {
            GameObject obj = Instantiate(wormPrefabs);
            obj.transform.SetParent(m_content.transform, false);
            obj.SetActive(false);
            pooledObjectWorm.Add(obj);
        }
        //
        for (int i = 0; i < amountFlyToPool; i++)
        {
            GameObject obj = Instantiate(flyPrefabs);
            obj.transform.SetParent(m_content.transform, false);
            obj.SetActive(false);
            pooledObjectFly.Add(obj);
        }
        //
        for (int i = 0; i < amountBulletEnemyToPool; i++)
        {
            GameObject obj = Instantiate(bulletEnemyPrefabs);
            obj.transform.SetParent(m_content.transform, false);
            obj.SetActive(false);
            pooledObjectBulletEnemy.Add(obj);
        }
        //
        //for (int i = 0; i < amountEffect; i++)
        //{
        //    GameObject obj = Instantiate(effectPrefabs);
        //    obj.SetActive(false);
        //    pooledObjectEffect.Add(obj);
        //}
    }

    // Update is called once per frame
  
    public GameObject GetPooledObjectWorm()
    {
      if(isActive)
        {
            for (int i = 0; i < pooledObjectWorm.Count; i++)
            {
                if (!pooledObjectWorm[i].activeInHierarchy)
                {
                    return pooledObjectWorm[i];
                }
            }
            
        }
        return null;
    }
    public GameObject GetPooledObjectFly()
    {
       if(isActive)
        {
            for (int i = 0; i < pooledObjectFly.Count; i++)
            {
                if (!pooledObjectFly[i].activeInHierarchy)
                {
                    return pooledObjectFly[i];
                }
            }
        }
        return null;
    }
    public GameObject GetPooledObjectBulletEnemy()
    {
      if(isActive)
        {
            for (int i = 0; i < pooledObjectBulletEnemy.Count; i++)
            {
                if (!pooledObjectBulletEnemy[i].activeInHierarchy)
                {
                    return pooledObjectBulletEnemy[i];
                }
            }
        }
        return null;
    }
    public GameObject GetPooledObjectCoin()
    {
       if(isActive)
        {
            for (int i = 0; i < pooledObjectCoin.Count; i++)
            {
                if (!pooledObjectCoin[i].activeInHierarchy)
                {
                    return pooledObjectCoin[i];
                }
            }
        }
        return null;
    }
     public GameObject GetPooledObjectEffect()
    {
       if(isActive)
        {
            for (int i = 0; i < pooledObjectEffect.Count; i++)
            {
                if (!pooledObjectEffect[i].activeInHierarchy)
                {
                    return pooledObjectEffect[i];
                }
            }
        }
        return null;
    }
    private void OnActiveGame(bool active)
    {
        isActive = active;
        if (!active)
           ResetGameObject();
    }
    private void ResetGameObject()
    {
        //Debug.Log("resetObj----poll");
        foreach (var obj in pooledObjectBulletEnemy)
            obj.SetActive(false);
        foreach (var obj in pooledObjectCoin)
            obj.SetActive(false);
        foreach (var obj in pooledObjectFly)
            obj.SetActive(false);
        foreach (var obj in pooledObjectWorm)
            obj.SetActive(false);
    }
}
