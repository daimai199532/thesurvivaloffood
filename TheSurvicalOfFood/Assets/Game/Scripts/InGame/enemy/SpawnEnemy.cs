using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;


public class SpawnEnemy : MonoBehaviour
{
  
    private bool m_isActive = false;
    private void Awake()
    {
        GameController.activeGame += OnActive;
    }
    private void OnDestroy()
    {
        GameController.activeGame -= OnActive;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnActive(bool isActive)
    {
        m_isActive = isActive;
        if(isActive)
        {
            StopAllCoroutines();
            StartCoroutine(SpawnEnemys(3f, 8, 2));
        }  
        else
            StopAllCoroutines();
       // Debug.Log(isActive + "--spawnEnemy");
    }
    IEnumerator SpawnEnemys(float waitSpawn, int amountWorm, int amountFly)
    {
        yield return new WaitForSeconds(2f);
        int count = 0;
        for (int i = 0; i < amountWorm; i++)
        {
            GameObject enemy = ObjectPoolEnemy.instance.GetPooledObjectWorm();
            if (count > 19)
                StopAllCoroutines();
           
            if (enemy != null)
            {
               // Debug.Log("run");
                count++;
                Vector2 pos;
                pos.x = Random.RandomRange(-3, 3);
                pos.y = Random.RandomRange(-5, 5);
                enemy.transform.position = new Vector2(pos.x, pos.y);
                enemy.transform.localScale = new Vector2(0.2f, 0.2f);
                //
                // StartCoroutine(CreateEffect(enemy.transform));
                //
                enemy.SetActive(true);
                enemy.GetComponent<ObjectWorm>().Init();
   
            }
        }
        for (int i = 0; i < amountFly; i++)
        {
            GameObject enemy = ObjectPoolEnemy.instance.GetPooledObjectFly();
            
            //if (count > 19)
            //    StopAllCoroutines();
            if (enemy != null)
            {
                
                count++;
                Vector2 pos;
                pos.x = Random.RandomRange(-1, 1);
                pos.y = Random.RandomRange(-5, 5);

               //
                enemy.transform.position = new Vector2(pos.x, pos.y);
                enemy.transform.localScale = new Vector2(0.2f, 0.2f);
                //
              
                //
                enemy.SetActive(true);
                enemy.GetComponent<ObjectFly>().Init();
            }
        }
        yield return new WaitForSeconds(waitSpawn);
        //GameController.UpdateTargetWeapon();
       
        StartCoroutine(SpawnEnemys(waitSpawn,amountWorm, amountFly));
    }
   
}
[Serializable]
public enum TypeEnemy
{
    worm,
    fly
}

