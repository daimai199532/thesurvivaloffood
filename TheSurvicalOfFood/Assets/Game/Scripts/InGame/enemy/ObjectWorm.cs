using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using TMPro;
using DG.Tweening;

public class ObjectWorm : ObjectBase
{
    [SerializeField] private BoxCollider2D m_boxCollider2d;
    [SerializeField] private ParticleSystem m_effectStart;
    [SerializeField] private ParticleSystem m_effectStart1;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_textDamageValue;
    [SerializeField] private GameObject m_objCoin;
    public int damagePlayer;

    private float m_health = 50;
    private Player m_player;
    private Transform m_target;

    private bool isRun = false;
    private Vector2 m_curent = Vector2.zero;
    private ObjectBase m_objectBase;
    
    //private void OnEnable()
    //{
       
    //    m_spine.AnimationState.SetAnimation(0, "run", true);
    //   // m_effectStart.GetComponent<ParticleSystem>().DORestart(false);
        
    //}

    private void OnDisable()
    {
        m_spine.AnimationState.SetAnimation(0, "run", true);
        StopAllCoroutines();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        
        m_objectBase = GetComponent<ObjectBase>();
        m_player = GameObject.FindGameObjectWithTag(GameConstant.TAG_PLAYER).GetComponent<Player>();
        if(m_player !=null)
             m_target = m_player.transform;

        
    }

    // Update is called once per frame
    void Update()
    {
        if (m_target != null && isRun)
            transform.position = Vector2.SmoothDamp(transform.position, m_target.position, ref m_curent, 5f , 5f );
        if (transform.position.x - m_target.position.x < 0)
            transform.localScale = new Vector3(1, 1, 1);
        else
            transform.localScale = new Vector3(-1, 1, 1);
    }
    public void Init()
    {
        m_health = base.health;
        isRun = false;
        m_spine.gameObject.SetActive(false);
        m_boxCollider2d.enabled = false;
        m_effectStart.gameObject.SetActive(false);
        m_effectStart1.gameObject.SetActive(false);
        StartCoroutine(WaitOnStart());
    }
    IEnumerator WaitOnStart()
    {
        yield return null;
        m_effectStart1.transform.position = transform.position;
        m_effectStart.transform.position = transform.position;
        m_effectStart.gameObject.SetActive(true);
        m_effectStart.Play();
        
       // yield return new WaitForSeconds(2f);
        yield return new WaitForSeconds(1.5f);
        m_effectStart1.gameObject.SetActive(true);
        m_effectStart1.Play();
        //
        m_spine.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        // yield return new WaitForSeconds(1f);
        m_boxCollider2d.enabled = true;
        isRun = true;
    }
    IEnumerator Dead()
    {
        yield return null;
        GameController.UpdateExpLevelStar(1);
        TrackEntry trackEntry = m_spine.AnimationState.SetAnimation(0, "dead", false);
        //
        GameObject coin = Instantiate(m_objCoin, transform.parent);
        coin.transform.position = transform.position;
        coin.gameObject.SetActive(true);
        //
        yield return new WaitForSeconds(trackEntry.AnimationEnd);

        StopAllCoroutines();
        gameObject.SetActive(false);
       

    }
    private void OnHurt(int valueDamage)
    {
        OnText(valueDamage);
        if(valueDamage - 1 > m_health)
        {
            m_objectBase.Dead(true);
            StartCoroutine(Dead());
        }
        m_health = m_health - valueDamage;
    }
    private void OnText(int valueDamage)
    {
        GameObject m_text = Instantiate(m_textDamageValue);
        TextMeshPro text = m_text.GetComponent<TextMeshPro>();
        text.text = valueDamage.ToString();
        m_text.transform.position = m_textDamageValue.transform.position;
        m_text.SetActive(true);
        m_text.transform.SetParent(transform.parent, true);
        m_text.transform.localScale = new Vector3(1, 1, 1);// new Vector3(transform.localScale.x, 1, 1);
        //m_text.transform.localEulerAngles = Vector3.zero;
        m_text.transform.DOMoveY(m_text.transform.position.y + .3f, 0.5f).OnComplete(() => {
            text.DOFade(0, 0.5f).OnComplete(() => {
                m_text.transform.DOKill();
                text.DOKill();
                Destroy(m_text);
            });
        });
    }
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == GameConstant.TAG_OBJECT_BULLET)
        {
            
            OnHurt(MainModel.damagePlayer);
            base.Hurt(MainModel.damagePlayer);
            
            //StartCoroutine(Dead());
            //Debug.Log(m_objectBase.isDead+"======================");
        }
        if (collider.tag == GameConstant.TAG_PLAYER)
        {
            //Debug.Log("colider Player");
            GameController.UpdateHealth(-damagePlayer, false);
        }
           

    }
}
