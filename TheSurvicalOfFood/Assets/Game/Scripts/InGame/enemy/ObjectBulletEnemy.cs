using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ObjectBulletEnemy : MonoBehaviour
{
    private int m_damageBullet = 10;
    private void OnEnable()
    {
        GameController.restartGame += OnDestroyObj;
        GameController.finishEvent += OnDestroyObj;
    }
    private void OnDestroy()
    {
        GameController.restartGame -= OnDestroyObj;
        GameController.finishEvent -= OnDestroyObj;
    }
    private void OnDestroyObj()
    {
        Destroy(gameObject);
    }
    public void Init(Transform target)
    {
        Vector2 direction = (target.transform.position - transform.position);
        gameObject.SetActive(true);
        transform.DOMove((Vector2)transform.position + direction.normalized * 30f, 20f).OnComplete(()=> {
            transform.DOKill();
            Destroy(gameObject);
            //gameObject.SetActive(false);
        });
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        //if (collider.tag != GameConstant.TAG_WALL)
        //    Destroy(gameObject);
        if (collider.tag != GameConstant.TAG_PLAYER)
            return;
        GameController.UpdateHealth(-m_damageBullet);
        Destroy(gameObject);
        //gameObject.SetActive(false);
    }
}
