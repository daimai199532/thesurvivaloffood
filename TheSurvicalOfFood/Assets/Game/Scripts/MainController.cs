using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController 
{
   
    public static void UpdateCoin(int coin)
    {
        MainModel.UpdateCoin(coin);
    }
    public static void UpdateDay()
    {
        MainModel.UpdateDay();
    }
    public static void UpdateStar(int level)
    {
        MainModel.UpdateStar(level);
    }
}
