using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstant 
{
    public static int COIN_RATIO = 5;


    //=====================scene===================
    public const string SCENE_HOME = "Home";
    public const string SCENE_GAME = "InGame";
    public const string SCENE_LOADING = "Loading";

    // ==============tag========================
    public const string TAG_WALL = "Wall";
    public const string TAG_MAGNET= "Magnet";
    public const string TAG_PLAYER = "Player";
    public const string TAG_ALLY = "Ally";
    public const string TAG_ENEMY_1 = "Enemy1";
    public const string TAG_MINION_2 = "Minion-2";
    public const string TAG_MINION_3 = "Minion-3";
    public const string TAG_MINION_4 = "Minion-4";
    public const string TAG_MINION_5 = "Minion-5";
    public const string TAG_BOSS_1 = "Boss-1";
    public const string TAG_BOSS_2 = "Boss-2";
    public const string TAG_BOSS_3 = "Boss-3";
    public const string TAG_BOSS_4 = "Boss-4";
    public const string TAG_OBJECT_BULLET = "Bullet";
    public const string TAG_OBJECT_PILLAR = "ObjectPillar";
    public const string TAG_OBJECT_SHIELD = "ObjectShield";
    public const string TAG_OBJECT_FOOD = "ObjectFood";
    public const string TAG_OBJECT_BOOM = "ObjectBoom";
    public const string TAG_OBJECT_TREASURE = "ObjectTreasure";
    public const string TAG_OBJECT_WEAPON = "Weapon";
    public const string TAG_OBJECT_WEAPON_DEFAULT = "Weapon-default";
    public const string TAG_OBJECT_KEY = "ObjectKey";
    public const string TAG_KEY_COUNT = "KeyCount";
    public const string TAG_TREASURE = "Treasure";

    // ========== PlayerPref ===========
    public const string PLAYER_PREF_MAP_LEVEL = "map-level";

    //====================animation=====================
    public const string ANIM_PLAYER_ATTACK = "";
    public const string ANIM_PLAYER_IDLE_INGAME = "idle_ingame";
    public const string ANIM_PLAYER_IDLE_INHOME = "idle_inhome";
    public const string ANIM_PLAYER_RUN = "run";
    public const string ANIM_PLAYER_WIN = "win";
}
