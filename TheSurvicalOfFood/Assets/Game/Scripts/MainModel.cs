using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text;

public class MainModel 
{
    // In Game
    public static int levelDay;
    public static int levelCoin;
    public static int levelStar;
    public static int levelExpStar;

    // current
    public static LevelResult levelResult;
    public static int[] arrWeaponTotals { get; private set; }
    public static int[] arrItemTotals { get; private set; }
    public static int m_totalStar { get; private set; }
    public static int m_totalCoin { get; private set; }
    public static int m_totalDay { get; private set; }
    public static int m_currentStar { get; private set; }
    public static int m_currentExpStar { get; private set; }
   
    public static int m_maxHealth { get; private set; }
    public static int m_currentHealth { get; private set; }
    //
    public static int amorPlayer { get; private set; }
    public static int criticalChancePlayer { get; private set; }
    public static int criticalDamagePlayer { get; private set; }
    public static int damagePlayer { get; private set; }
    //
    public static bool isAmorPlayer = false;
    public static bool isCriticalChancePlayer = false;
    public static bool isCriticalDamagePlayer = false;
    public static void InitGameInfo()
    {
        levelDay = 0;
        levelCoin = 0;
        levelStar = 0;
        levelExpStar = 0;
        
        if (m_currentHealth < 1)
        {
            //m_currentHealth = 100;
            UpdateHealth(100,false);
            m_totalCoin = 0;
            //GameController.UpdateHealth(100);
            
        }
            
            //Debug.Log(m_currentHealth);
    }
    public static void LoadDataUser()
    {
        // array weapons
        string[] weapons = PlayerPrefs.GetString("weapon_Player", "0,-1,-1,-1,-1,-1").Split(',');
        arrWeaponTotals = new int[6];
        for(int i=0;i< weapons.Length;i++)
        {
            arrWeaponTotals[i] = int.Parse(weapons[i]);
        }

        // array item
        string[] items = PlayerPrefs.GetString("item_Player", "-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1").Split(',');
        arrItemTotals = new int[12];
        for (int i = 0; i < items.Length; i++)
        {
            arrItemTotals[i] = int.Parse(items[i]);
        }
        //
        m_totalCoin = PlayerPrefs.GetInt("coin_Player", 0);
        m_totalDay = PlayerPrefs.GetInt("day_Player", 0);
        m_totalStar = PlayerPrefs.GetInt("star_Player", 0);
        m_currentHealth = PlayerPrefs.GetInt("current_health_Player", 100);
        m_maxHealth = PlayerPrefs.GetInt("max_health_Player", 100);
        m_currentStar = PlayerPrefs.GetInt("current_start_Player", 0);
        m_currentExpStar = PlayerPrefs.GetInt("current_exp_start_Player", 0);
        //
        amorPlayer = PlayerPrefs.GetInt("amorPlayer", 10);
        criticalChancePlayer = PlayerPrefs.GetInt("criticalChancePlayer", 5);
        criticalDamagePlayer = PlayerPrefs.GetInt("criticalDamagePlayer", 50);
        damagePlayer = PlayerPrefs.GetInt("damagePlayer", 76);
       // Debug.Log(m_currentHealth);
        //Debug.Log(m_maxHealth);
    }
    public static void SaveData()
    {
        // weapon
        StringBuilder arr = new StringBuilder();
        foreach(int s in arrWeaponTotals)
        {
            arr.Append(s).Append(",");
        }
        if(arr.Length > 0)
        {
            arr.Remove(arr.Length - 1, 1);
            PlayerPrefs.SetString("weapon_Player", arr.ToString());
        }

        //item
        arr = new StringBuilder();
        foreach (int s in arrItemTotals)
        {
            arr.Append(s).Append(",");
        }
        if (arr.Length > 0)
        {
            arr.Remove(arr.Length - 1, 1);
            PlayerPrefs.SetString("item_Player", arr.ToString());
        }

        //
        PlayerPrefs.SetInt("coin_Player", m_totalCoin);
        PlayerPrefs.SetInt("level_Player", m_totalDay);
        PlayerPrefs.SetInt("current_health_Player", m_currentHealth);
        PlayerPrefs.SetInt("max_health_Player", m_maxHealth);
        PlayerPrefs.SetInt("current_start_Player", m_currentStar);
        PlayerPrefs.SetInt("current_exp_start_Player", m_currentExpStar);
        //
        PlayerPrefs.SetInt("amorPlayer", amorPlayer);
        PlayerPrefs.SetInt("criticalChancePlayer", criticalChancePlayer);
        PlayerPrefs.SetInt("criticalDamagePlayer", criticalDamagePlayer);
        PlayerPrefs.SetInt("damagePlayer", damagePlayer);
        //
        PlayerPrefs.Save();
    }

    public static void UpdateHealth(int valueHealth, bool maxHealth)
    {
        if (maxHealth)
            m_maxHealth += valueHealth;
        else
            m_currentHealth += valueHealth;
        if (m_currentHealth > m_maxHealth - 1)
        {
            m_currentHealth = m_maxHealth;
        }
        if (m_currentHealth < 1)
            m_currentHealth = 0;
        //
        SaveData();
    }
    public static void UpdateCoin(int valueCoin)
    {
        m_totalCoin += valueCoin;
        SaveData();
    }
  
    public static void UpdateDay()
    {
        m_totalDay ++;
        //
        SaveData();
    }
    public static void UpdateStar(int level)
    {
        m_currentStar += level;
        //
        SaveData();
    }
    public static void UpdateExpStar(int level)
    {
        m_currentExpStar = level;
        //
        SaveData();
    }
    public static void UpdateProperties(int index, int value)
    {

        switch (index.ToString())
        {
            case "0":
                amorPlayer += value;
                //isAmorPlayer = true;
                SetIsChangeProperties(isAmorPlayer.ToString(), true);
                //Debug.Log(amorPlayer + "--MainModel");
                break;
            case "1":
                criticalChancePlayer += value;
                //isCriticalChancePlayer = true;
                SetIsChangeProperties(isCriticalChancePlayer.ToString(), true);

                break;
            case "2":
                criticalDamagePlayer += value;
                //isCriticalDamagePlayer = true;
                SetIsChangeProperties(isCriticalDamagePlayer.ToString(), true);
                break;
        }
        //Debug.Log(index + "--MainModel");
        //Debug.Log(value + "--MainModel");
        //Debug.Log(amorPlayer + "--MainModel");
        //Debug.Log(criticalChancePlayer + "--MainModel");
        //Debug.Log(criticalDamagePlayer + "--MainModel");
        SaveData();
    }
    public static string GetIsChangeProperties(string name)
    {
        return PlayerPrefs.GetString("IsProperties " + name, "false"); 
    }
    public static void SetIsChangeProperties(string name, bool active)
    {
        PlayerPrefs.SetString("IsProperties " + name, active.ToString());
        //
        PlayerPrefs.Save();
    }
    public static void BuyWeapon(int index, int pos,bool isBuy) // khi buy (dk: if pos = -1 -> tu sap xep theo vi tri tiep theo, else sap xep theo pos
    { // swap data
        if (isBuy)
        {
            if(pos == -1) // tim vi tri trong roi sap vao
            {
                for(int i= 0;i < arrWeaponTotals.Length;i++)
                {
                    if(arrWeaponTotals[i] < 0)
                    {
                        arrWeaponTotals[i] = index;
                        break;
                    }
                }
            }
            else
                arrWeaponTotals[pos] = index;
            SaveData();
        }
        else
        {
            arrWeaponTotals[pos] = -1;
            SaveData();
        }
    }
    public static void BuyItem(int index, int pos, bool isBuy) 
    { // swap data
        if (isBuy)
        {
            if (pos == -1) // tim vi tri trong roi sap vao
            {
                for (int i = 0; i < arrItemTotals.Length; i++)
                {
                    if (arrItemTotals[i] < 0)
                    {
                        arrItemTotals[i] = index;
                        break;
                    }
                }
            }
            else
                arrItemTotals[pos] = index;
            SaveData();
        }
        else
        {
            arrItemTotals[pos] = -1;
            SaveData();
        }
    }

}
public class LevelResult
{
    public bool isComplete; //check level win
    public bool isFinish; // check finish game
    public int levelDay;
    public int levelCoin;
    public int levelStar;

    public LevelResult()
    {
        isComplete = false;
        isFinish = false;
    }
}
