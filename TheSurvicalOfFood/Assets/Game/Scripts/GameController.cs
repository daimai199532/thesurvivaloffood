using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public  class GameController 
{
    public static Action<int, bool> updateWeaponUIEvent;
    public static Action<int, bool> updateItemUIEvent;
    public static Action<int,bool> updateHealthEvent;
    public static Action<int> updateCoinEvent;
    public static Action<int> updateExpStarLevelEvent;
    public static Action<int> updateMapLevelEvent;
    public static Action lootItemEvent;
    //
    public static Action<int, int> updatePropertiesEvent;
    //
    public static Action finishEvent;
    public static Action<bool> activeGame;
    public static Action restartGame;
    public static Action<bool> finishGamePlaySceneEvent;
    public static Action reviveEvent;
    public static Action stopTimeCountDown;
    //
    public static Action targetWeaponEvent;
    public static Action<bool> attackEvent;
    public static Action<bool> movementEvent;
    public static Action<Vector3> targetPlayer;
    //

    public static void UpdateTargetWeapon()
    {
        targetWeaponEvent?.Invoke();
    }
    public static void UpdateAttackEnemy(bool isOn)
    {
        attackEvent?.Invoke(isOn);
    }
    public static void UpdateMovement(bool isRun)
    {
        movementEvent?.Invoke(isRun);
    }
    public static void UpdateHealth(int valueHealth, bool maxHealth = false)
    {
      
        MainModel.UpdateHealth(valueHealth, maxHealth);
        updateHealthEvent?.Invoke(valueHealth, maxHealth);
        //Debug.Log(MainModel.m_currentHealth);
    }
    public static void UpdateLevelCoin(int valueCoin)
    {
        MainModel.levelCoin += valueCoin;
        updateCoinEvent?.Invoke(valueCoin);
    }
    public static void UpdateExpLevelStar(int value)
    {
        int expLevelStar = 10;
        MainModel.levelExpStar += value;
        if (MainModel.levelExpStar > expLevelStar)
        {
            MainModel.levelExpStar = 0;
            MainModel.levelStar++;
        }
        updateExpStarLevelEvent?.Invoke(value);
    }
  
    public static void UpdateLeveDay(int day)
    {
        MainModel.levelDay += day;
        updateMapLevelEvent?.Invoke(day);
    }
    public static void OnLootItem()
    {
        lootItemEvent?.Invoke();
    }
   
    public static void OnActiveGame(bool isActive)
    {
        //Debug.Log(isActive + "===active");
        activeGame?.Invoke(isActive);
    }
   
   public static void CompleteLevel()
    {
        MainModel.UpdateCoin(MainModel.levelCoin);
        MainModel.UpdateDay();
        MainModel.UpdateStar(MainModel.levelStar);
        MainModel.UpdateExpStar(MainModel.levelExpStar);
      //  Debug.Log("[DEBUG] - CompleteLevel - finishGamePlaySceneEvent");
        finishGamePlaySceneEvent?.Invoke(true);
       
    }
    public static void FailLevel()
    {
      
        //restartGame?.Invoke(); // bug hien tai dang o dau
        finishGamePlaySceneEvent?.Invoke(false);
        
    }
    public static void Finish()
    {
        //OnActiveGame(false);
        finishEvent?.Invoke();
    }
    public static void StartNemGame() // lose complete
    {
        activeGame?.Invoke(true);
        //restartGame?.Invoke();
    }
    public static void ReStartGame()
    {
        restartGame?.Invoke();
    }
    public static void Revive()
    {
        reviveEvent?.Invoke();
    }
    public static void StopTimeCountDown()
    {
        stopTimeCountDown?.Invoke();
    }
    public static void TargetPlayer(Vector3 pos)
    {
        targetPlayer?.Invoke(pos);
    }
    public static void  UpdateProperties(int index, int value)
    {
        //Debug.Log("GameControll " + value);
        MainModel.UpdateProperties(index, value);
        updatePropertiesEvent?.Invoke(index, value);
    }
    public static void BuyWeapon(int index, int pos, bool isbuy)
    {
       
        if (index < 3) // index == so luong sprites (weapon + item)
        {
            //Debug.Log(index + "========GameCon");
            MainModel.BuyWeapon(index, pos, isbuy);
            updateWeaponUIEvent?.Invoke(index, isbuy);
        }
        else
        {
            //Debug.Log(index + "========GameCon");
            MainModel.BuyItem(index, pos, isbuy);
            updateItemUIEvent?.Invoke(index, isbuy);
        }
        
    }
}
