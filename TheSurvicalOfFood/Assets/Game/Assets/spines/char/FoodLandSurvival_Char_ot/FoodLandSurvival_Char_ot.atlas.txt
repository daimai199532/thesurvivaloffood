
FoodLandSurvival_Char_ot.png
size: 852,416
format: RGBA8888
filter: Linear,Linear
repeat: none
body
  rotate: false
  xy: 2, 106
  size: 208, 308
  orig: 208, 308
  offset: 0, 0
  index: -1
body_2
  rotate: true
  xy: 422, 245
  size: 169, 142
  orig: 170, 143
  offset: 0, 0
  index: -1
body_s
  rotate: false
  xy: 212, 106
  size: 208, 308
  orig: 208, 308
  offset: 0, 0
  index: -1
eye_1_L
  rotate: true
  xy: 658, 252
  size: 70, 51
  orig: 71, 51
  offset: 0, 0
  index: -1
eye_1_R
  rotate: true
  xy: 691, 162
  size: 88, 56
  orig: 88, 56
  offset: 0, 0
  index: -1
eye_1_s_L
  rotate: true
  xy: 349, 13
  size: 91, 74
  orig: 91, 75
  offset: 0, 0
  index: -1
eye_1_s_R
  rotate: true
  xy: 539, 8
  size: 109, 79
  orig: 109, 79
  offset: 0, 0
  index: -1
eye_2_L
  rotate: false
  xy: 700, 73
  size: 42, 44
  orig: 42, 45
  offset: 0, 1
  index: -1
eye_2_R
  rotate: false
  xy: 700, 21
  size: 45, 50
  orig: 46, 50
  offset: 0, 0
  index: -1
eye_3_L
  rotate: false
  xy: 711, 260
  size: 79, 62
  orig: 79, 63
  offset: 0, 0
  index: -1
eye_3_R
  rotate: true
  xy: 150, 7
  size: 97, 67
  orig: 97, 67
  offset: 0, 0
  index: -1
eye_happy
  rotate: false
  xy: 747, 45
  size: 46, 48
  orig: 46, 48
  offset: 0, 0
  index: -1
eye_hit
  rotate: true
  xy: 792, 196
  size: 45, 54
  orig: 46, 54
  offset: 1, 0
  index: -1
eyebrow_L
  rotate: false
  xy: 566, 247
  size: 90, 75
  orig: 90, 75
  offset: 0, 0
  index: -1
eyebrow_R
  rotate: true
  xy: 422, 110
  size: 133, 77
  orig: 133, 78
  offset: 0, 1
  index: -1
eyebrow_s_L
  rotate: true
  xy: 2, 2
  size: 102, 87
  orig: 102, 87
  offset: 0, 0
  index: -1
eyebrow_s_R
  rotate: false
  xy: 700, 324
  size: 145, 90
  orig: 145, 90
  offset: 0, 0
  index: -1
foot
  rotate: true
  xy: 219, 11
  size: 93, 56
  orig: 96, 58
  offset: 1, 1
  index: -1
foot_s
  rotate: true
  xy: 621, 140
  size: 105, 68
  orig: 108, 70
  offset: 1, 1
  index: -1
hair_1
  rotate: true
  xy: 745, 95
  size: 53, 80
  orig: 53, 80
  offset: 0, 0
  index: -1
hair_2
  rotate: true
  xy: 620, 8
  size: 109, 78
  orig: 109, 78
  offset: 0, 0
  index: -1
hair_2_s
  rotate: false
  xy: 566, 324
  size: 121, 90
  orig: 121, 90
  offset: 0, 0
  index: -1
hair_3
  rotate: true
  xy: 749, 150
  size: 44, 87
  orig: 46, 87
  offset: 2, 0
  index: -1
hair_3_s
  rotate: false
  xy: 91, 5
  size: 57, 99
  orig: 58, 99
  offset: 1, 0
  index: -1
hair_4
  rotate: true
  xy: 792, 243
  size: 79, 58
  orig: 89, 58
  offset: 1, 0
  index: -1
hair_4_s
  rotate: true
  xy: 277, 12
  size: 92, 70
  orig: 92, 70
  offset: 0, 0
  index: -1
hair_5
  rotate: false
  xy: 425, 2
  size: 112, 106
  orig: 112, 106
  offset: 0, 0
  index: -1
hair_5_s
  rotate: true
  xy: 501, 119
  size: 124, 118
  orig: 124, 118
  offset: 0, 0
  index: -1
mouth
  rotate: false
  xy: 691, 119
  size: 52, 41
  orig: 52, 42
  offset: 0, 0
  index: -1
mouth_ag
  rotate: true
  xy: 749, 206
  size: 52, 41
  orig: 52, 42
  offset: 0, 0
  index: -1
